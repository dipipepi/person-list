import React, { Component } from 'react';
import "./App.css";
import "bootswatch/darkly/bootstrap.css";
import {Person} from "./Components/Person.js";
import {AddPerson} from "./Components/AddPerson.js";
import PropTypes from 'prop-types';




class App extends Component {

    constructor(props){
        super(props);
        this.state = {person : this.props.persons};
        this.props = this.state.person;
    }

    //persons = this.state.person;

    addPerson(person){
        this.props.persons.push(person);
        this.setState({
            person: this.props.persons
        });
    }

    render() {
        return (
            <div className="container main-block">
                <div className="row">
                    <div className="col-xs-12 col-md-6">
                        <h2>Person list</h2>
                        {this.props.persons.map((item, id)=><Person key={id} person={item}/>)}
                    </div>
                    <div className="col-xs-12 col-md-6 form_adding">
                        <h2>Add person</h2>
                        <AddPerson addPerson={this.addPerson.bind(this)}/>
                    </div>
                </div>
            </div>
        );
    }
}

App.PropTypes = {
    persons : PropTypes.object
};

export default App;
