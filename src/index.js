import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

var persons = [{
    name: "Ivan",
    lastName : "Ivanov",
    position : "Manager",
    age : "27"
}];

ReactDOM.render(<App persons={persons}/>, document.getElementById('root'));
registerServiceWorker();
