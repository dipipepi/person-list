import React, { Component } from 'react';

export class AddPerson extends React.Component{

    person = {
        name : "",
        lastName : "",
        position: "",
        age : ""
    };

    onChangeName(e){
        e.preventDefault();
        this.person.name = e.target.value;
    }

    onChangeLastName(e){
        e.preventDefault();
        this.person.lastName = e.target.value;
    }

    onChangePosition(e){
        e.preventDefault();
        this.person.position = e.target.value;
    }

    onChangeAge(e){
        e.preventDefault();
        console.log();
        this.person.age = e.target.value;
    }

    add(){
        console.log(this.person);
        this.props.addPerson(this.person);
        this.person = {
            name : "",
            lastName : "",
            position: "",
            age : ""
        };
        //document.getElementById('name').value = '';
        //document.getElementById('lastName').value = '';
        console.log(this.person);
    }

    render(){
        return(
            <form>
                <div className="form-group">
                    <input
                        onChange={this.onChangeName.bind(this)}
                        type="text" className="form-control"
                        placeholder="Name"
                        name="name"
                        id="name"
                    />

                </div>
                <div className="form-group">
                    <input
                        onChange={this.onChangeLastName.bind(this)}
                        type="text" className="form-control"
                        placeholder="Lastname"
                        name="lastname"
                        id="lastName"
                    />
                </div>
                <div className="form-group">
                    <input
                        onChange={this.onChangePosition.bind(this)}
                        type="text" className="form-control"
                        placeholder="Position"
                        name="position"
                        id="position"
                    />
                </div>
                <div className="form-group">
                    <input
                        onChange={this.onChangeAge.bind(this)}
                        type="text" className="form-control"
                        placeholder="Age"
                        name="age"
                        id="age"
                    />
                </div>
                <button
                    onClick={this.add.bind(this)}
                    type="reset"
                    className="btn btn-default">Add</button>
            </form>
        );
    }
}