import React, { Component } from 'react';

export class Person extends React.Component{


    render(){
        return(
            <div className="row">
                <div className="col-xs-12 user">
                    <p><span>Name: </span> {this.props.person.name}</p>
                    <p><span>Last name:</span> {this.props.person.lastName}</p>
                    <p><span>Position: </span> {this.props.person.position}</p>
                    <p><span>Age: </span> {this.props.person.age}</p>
                </div>
            </div>
        );
    }
}